import math, strformat

type
    Num* = ref object of RootObj
        val*: float64
        drv*: float64

proc `$`*(x: Num): string =
    &"Num({x.val}, {x.drv})"

proc `-`*(n: Num): Num =
    Num(val: -n.val, drv: -n.drv)

proc `+`*(x: Num, y: Num): Num = 
    Num(
        val: x.val + y.val, 
        drv: x.drv + y.drv
        )

# adding a scalar does not change the derivative
proc `+`*(x: Num, y: float64): Num = 
    Num(
        val: x.val + y, 
        drv: x.drv
        )

# adding a scalar does not change the derivative
proc `+`*(x: float64, y: Num): Num = 
    Num(
        val: x + y.val, 
        drv: y.drv
        )

proc `-`*(x: Num, y: Num): Num = 
    Num(
        val: x.val - y.val, 
        drv: x.drv - y.drv
        )

proc `-`*(x: Num, y: float64): Num = 
    Num(
        val: x.val - y, 
        drv: x.drv
        )


proc `-`*(x: float64, y: Num): Num = 
    -y + x

proc `*`*(x: Num, y: Num): Num = 
    Num(
        val: x.val * y.val, 
        drv: x.drv * y.val + x.val * y.drv
        )

proc `*`*(x: Num, y: float64): Num = 
    Num(
        val: x.val * y, 
        drv: x.drv * y
        )

proc `*`*(x: float64, y: Num): Num = 
    Num(
        val: x * y.val, 
        drv: x * y.drv
        )

proc `/`*(x: Num, y: Num): Num = 
    Num(
        val: x.val / y.val,
        drv: x.drv / y.val - x.val * y.drv / (y.val * y.val)
    )

proc `/`*(x: Num, y: float64): Num =
    x * (1/y)

proc constNum*(x: float64): Num = 
    Num(val: x, drv: 0)

proc variable*(x: float64): Num =
    Num(val: x, drv: 1)

proc `/`*(x: float64, y: Num): Num =
    constNum(x) / y


proc recip*(this: Num): Num = 
    1.0 / this

proc `==`*(x: Num, y: Num): bool =
    x.val == y.val

proc `<`*(x: Num, y: Num): bool =
    x.val < y.val

proc `<=`*(x: Num, y: Num): bool =
    x.val <= y.val

proc `>=`*(x: Num, y: Num): bool =
    x.val >= y.val


let numInf* = Num(val: Inf, drv: 0.0)
let numNegInf* = Num(val: NegInf, drv: 0.0)
let numNan* = Num(val: NaN, drv: 0.0)
let numZero* = Num(val: 0.0, drv: 0.0)
let numNegZero* = Num(val: -0.0, drv: 0.0)
let numOne* = Num(val: 1.0, drv: 0)

proc isNan*(x: Num): bool =
    x.val == NaN or x.drv == NaN

proc isInf*(x: Num): bool = 
    x.val == Inf or x.drv == Inf or x.val == NegInf or x.drv == NegInf

proc isFinite*(x: Num): bool = 
    not isInf(x)

proc isNormal*(x: Num): bool =
    x.val.classify == fcNormal and x.drv.classify == fcNormal

proc abs*(x: Num): Num =
    if x.val >= 0.0:
        x
    else:
        -x

proc pow*(x: Num, f: float64): Num =
    Num(
        val: x.val.pow(f),
        drv: f * x.val.pow(f - 1) * x.drv
    )

proc pow*(x: Num, y: Num): Num = 
    let v = x.val.pow(y.val)
    Num(
        val: v,
        drv: v * (ln(x.val) * y.drv + y.val/x.val * x.drv)
    )

proc exp*(x: Num): Num = 
    pow(constNum(E), x)

proc exp2*(x: Num): Num =
    pow(constNum(2), x)

proc exp10*(x: Num): Num =
    pow(constNum(10), x)


    
proc log*(x: Num): Num =
    Num(
        val: ln(x.val),
        drv: x.drv / x.val
    )


proc logb*(b: float64, x: Num): Num =
    log(x) / log(constNum(b))

proc log2*(x: Num): Num =
    logb(2.0, x)

proc log10*(x: Num): Num =
    logb(10.0, x)

proc sqrt*(x: Num): Num =
    pow(x, 0.5)

const one_third* = 1.0/3.0

proc cbrt*(x: Num): Num = 
    pow(x, one_third)

proc hypot*(x: Num, y: Num): Num = 
    sqrt(
        pow(x, 2.0) + pow(y, 2.0)
    )

proc sin*(x: Num): Num =
    Num(
        val: sin(x.val),
        drv: cos(x.val) * x.drv
    )

proc cos*(x: Num): Num =
    Num(
        val: cos(x.val),
        drv: -sin(x.val) * x.drv
    )

proc tan*(x: Num): Num = 
    sin(x) / cos(x)

proc ctan*(x: Num): Num = 
    tan(x).recip

proc arcsin*(x: Num): Num =
    Num(
        val: arcsin(x.val),
        drv: sqrt(1.0 - x.val.pow(2.0)) * x.drv
    )

proc arctan*(x: Num): Num =
    Num(
        val: arctan(x.val),
        drv: sqrt(1.0 + x.val.pow(2.0)) * x.drv
    )


proc arccos*(x: Num): Num =
    Num(
        val: arccos(x.val),
        drv: -sqrt(1.0 - x.val.pow(2.0)) * x.drv
    )

proc sinh*(x: Num): Num = 
    Num(
        val: sinh(x.val),
        drv: cosh(x.val) * x.drv
    )

proc cosh*(x: Num): Num =
    Num(
        val: cosh(x.val),
        drv: sinh(x.val) * x.drv
    )

proc tanh*(x: Num): Num =
    let v: float64 = tanh(x.val)
    Num(
        val: v,
        drv: (1.0 - v.pow(2.0)) * x.drv
    )

proc arcsinh*(x: Num): Num =
    Num(
        val: arcsinh(x.val),
        drv: (x.val.pow(2.0) + 1.0) * x.drv
    )


proc arccosh*(x: Num): Num =
    Num(
        val: arccosh(x.val),
        drv: (x.val.pow(2.0) - 1.0) * x.drv
    )

proc arctanh*(x: Num): Num =
    Num(
        val: arctanh(x.val),
        drv: (1.0 - x.val.pow(2.0)) * x.drv
    )

# differentiate a single-variable function at x
proc diff*(f: proc(x:Num):Num {.closure.}, x: float64): float64 =
    let v = variable(x)
    f(v).drv

# calculates the gradient of a multivariable function at a certain point
# this is obviously inefficient because for n-var function f, it calculates f(x) n times just to get the gradient f'(x)
proc grad*(f: proc(x:seq[Num]):Num {.closure.}, xs: seq[float64]): seq[float64] =
    result = newSeq[float64]()
    var input = newSeq[Num]()
    for elem in xs:
        input.add(constNum(elem))
    for i in 0 ..< xs.len:
        input[i].drv = 1
        result.add(f(input).drv)
        input[i].drv = 0



