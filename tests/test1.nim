import nim_ffadpkg/ffadlib
import math
import sugar

proc constAndVar() =
    var v = variable(PI)
    var c = constNum(PI)
    doAssert(v.val == PI and v.drv == 1.0)
    doAssert(c.val == PI and c.drv == 0.0)
    v = variable(2.17)
    c = constNum(223.18)
    doAssert(v.val == 2.17   and v.drv == 1.0)
    doAssert(c.val == 223.18 and c.drv == 0.0)
    echo "### tested constants and variables"

template `~=`(x: typed, y: typed): bool =
    abs(x - y) < 1e-10

proc elementaryFunc() =
    var v: Num
    v = variable(2.1)
    var absv = abs(v)
    doAssert(absv.val == 2.1 and absv.drv == 1.0)
    v = variable(-2.1)
    absv = abs(v)
    doAssert(absv.val == 2.1) 
    doassert(absv.drv == -1.0)

    v = variable(3.2)
    var expv = exp(v)
    doAssert(expv.val ~= 24.532530197109352)
    doAssert(expv.drv ~= 24.532530197109352)
    var exp2v = exp2(v)
    doAssert(exp2v.val ~= 9.18958683997628)
    doAssert(exp2v.drv ~= exp2v.val * 0.6931471805599453)
    var exp10v = exp10(v)
    doAssert(exp10v.val ~= 1584.893192461114)
    doAssert(exp10v.drv ~= exp10v.val * 2.302585092994046)
    var logv = log(v)
    doAssert(logv.val ~= 1.1631508098056809)
    doAssert(logv.drv ~= 1.0 / 3.2)
    var log2v = log2(v)
    doAssert(log2v.val ~= 1.6780719051126378)
    doAssert(log2v.drv ~= log2(E) / v.val)
    var log10v = log10(v)
    doAssert(log10v.val ~= 0.505149978319906)
    doAssert(log10v.drv ~= log10(E) / v.val)

    v = variable(2.0)
    var sqrtv = sqrt(v)
    doAssert(sqrtv.val ~= 1.4142135623730951)
    doAssert(sqrtv.drv ~= 0.35355339059327373)
    var powv_half = pow(v, 0.5)
    doAssert(sqrtv.val ~= powv_half.val)
    doAssert(sqrtv.drv ~= powv_half.drv)

    var powv_2 = pow(v, 2.0)
    doAssert(powv_2.val ~= 4.0)
    doAssert(powv_2.drv ~= 4.0)

    var cbrtv = cbrt(v)
    doAssert(cbrtv.val ~= v.val.cbrt())
    doAssert(cbrtv.drv ~= 1.0/3.0 * v.val.pow(-2.0 / 3.0))

    v = variable(PI)
    var sinv = sin(v)
    var cosv = cos(v)
    var tanv = tan(v)
    var ctanv = ctan(v)
    doAssert(sinv.val < 1e-10 and sinv.drv == -1.0)
    doAssert(cosv.val == -1.0)
    doassert(cosv.drv < 1e-10)
    doAssert(tanv.val < 1e-10 and tanv.drv == 1.0 + tanv.val.pow(2.0))
    doAssert(ctanv.val < -1e10) 
    doassert(ctanv.drv < -1e10)

    v = variable(1.5)
    sinv = sin(v)
    cosv = cos(v)
    tanv = tan(v)
    ctanv = ctan(v)
    dump(v)
    dump(sin(v))
    dump(cos(v))
    dump(tan(v))
    dump(ctan(v))
    doAssert(sinv.val ~= 0.9974949866040544)
    doAssert(sinv.drv ~= cosv.val)
    doAssert(cosv.val ~= 0.0707372016677029)
    doassert(cosv.drv ~= -sinv.val)
    doAssert(tanv.val ~= 14.101419947171719)
    doAssert(tanv.drv ~= (1.0 + tanv.val.pow(2.0)))
    doAssert(ctanv.val ~= 1.0/tanv.val)
    doAssert(ctanv.drv ~= -(1.0 + ctanv.val.pow(2.0)))

    v = variable(PI / 4.0)
    sinv = sin(v)
    cosv = cos(v)
    tanv = tan(v)
    ctanv = ctan(v)
    dump(v)
    dump(sin(v))
    dump(cos(v))
    dump(tan(v))
    dump(ctan(v))
    doAssert(sinv.val ~= 0.7071067811865475)
    doAssert(sinv.drv ~= 0.7071067811865475)
    doAssert(cosv.val ~= 0.7071067811865475)
    doassert(cosv.drv ~= -0.7071067811865475)
    doAssert(tanv.val ~= 1.0)
    doAssert(tanv.drv ~= (1.0 + tanv.val.pow(2.0)))
    doAssert(ctanv.val ~= 1.0)
    doAssert(ctanv.drv ~= -(1.0 + ctanv.val.pow(2.0)))

    var v1 = variable(3.0)
    var v2 = constNum(4.0)
    var hypotv = hypot(v1, v2)
    doAssert(hypotv.val ~= 5.0)
    doAssert(hypotv.drv ~= 1.0 / (2.0 * hypotv.val) * 2.0 * v1.val)
    var tmp = v1
    v1 = v2
    v2 = tmp
    hypotv = hypot(v1, v2)
    doAssert(hypotv.val ~= 5.0)
    doAssert(hypotv.drv ~= 1.0 / (2.0 * hypotv.val) * 2.0 * v2.val)
    echo "### tested elementary functions"

proc test_chain_rule()=
    var v = variable(12.11)
    var v1 = sin(v).pow(2.0)
    doAssert(v1.val ~= sin(v.val).pow(2.0))
    doAssert(v1.drv ~= 2.0 * sin(v.val) * cos(v.val))
    v1 = cos(v).pow(2.0)
    doAssert(v1.val ~= cos(v.val).pow(2.0))
    doAssert(v1.drv ~= -2.0 * cos(v.val) * sin(v.val))
    echo "### tested chain rule"

constAndVar()
elementaryFunc()
test_chain_rule()
# echo $sin(v)
echo "### test1 passed"
